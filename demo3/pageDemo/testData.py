from pageDemo.readFile import ReadFile
from pageDemo.sqlRead import DBconnect
import os


class TestDmeo():
    # 评论文件地址
    fileDir = "E:\workspace\测试文档\集盒大学测试数据\课程评论(pro)"



    # 获取课程名称,查询课程id
    def getCourseName(self):
        courseInfo = {}

        readFile = ReadFile()
        db = DBconnect()

        fileNamePaths = readFile.getFileName(self.fileDir)

        for fileNamePath in fileNamePaths:
            fileName = os.path.basename(fileNamePath).split(".")[0]
            # print("课程名称：%s" % fileName)

            sql = "select course_id from jh_course where `status`='1' and title = '" + fileName + "'"

            # 连接数据查询课程id，并返回id列表
            dbc = db.dbConn()
            idList = db.search(dbc, sql)
            # print("课程id：%s" % idList)
            db.dbClose(dbc)

            courseInfo.setdefault(fileName, idList[0])
        return courseInfo

    # 获取课程对应的评论
    def getCourseComment(self):
        courseCommentInfo = {}
        readFile = ReadFile()

        fileNamePaths = readFile.getFileName(self.fileDir)
        for fileNamePath in fileNamePaths:
            fileName = os.path.basename(fileNamePath).split(".")[0]
            comList = readFile.readFile(fileNamePath)
            courseCommentInfo.setdefault(fileName, comList)
        return courseCommentInfo


if __name__ == "__main__":
    # # 日志文件地址
    # logPath = "E:\workspace\测试文档\集盒大学测试数据\log.txt"

    test = TestDmeo()
    readFile = ReadFile()

    # 获取课程id
    courseInfo = test.getCourseName()
    courseNames = courseInfo.keys()
    courseNameList = list(courseNames)

    # 获取课程评论
    courseCommentInfo = test.getCourseComment()
    num = 0
    for courseName in courseNameList:
        commentList = courseCommentInfo.get(courseName)
        print("每门课程对应的评论条数：%d" % len(commentList))
        num += len(commentList)
    print("总的评论条数：%d" % num)
    # for courseName in courseNameList:
        # print("课程名称：%s ；课程id：%s" % (courseName, courseInfo.get(courseName)))








