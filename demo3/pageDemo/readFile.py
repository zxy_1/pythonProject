import os


class ReadFile():
    # 读取文件内容
    def readFile(self, filePath):
        with open(filePath, 'r') as fp:
            re = fp.read()
            re = re.split("\n")
            fp.close()
        return re

    # 获取文件名称
    def getFileName(self, fileDirecter):
        fileNameList = []
        dir = os.path.isdir(fileDirecter)
        if dir:
            files = os.listdir(fileDirecter)
            for file in files:
                fileName = os.path.basename(file)
                fileName = os.path.join(fileDirecter, fileName)
                # print(fileName)
                fileNameList.append(fileName)
        else:
            print("文件目录错误！！！")

        return fileNameList

    # 写入文件内容
    def writeFile(self, filePath, message):
        with open(filePath, 'a') as fp:
            fp.write(message + "\n")
            fp.close()


if __name__ == "__main__":
    filePath = r"E:\workspace\测试文档\集盒大学测试数据\昵称.txt"
    cpFilePath = "E:\workspace\测试文档\集盒大学测试数据\log.txt"

    readFile = ReadFile()
    re = readFile.readFile(filePath)
    print(len(re))
    for r in range(0, 10):
        readFile.writeFile(cpFilePath, re[r])
        print(re[r])

    # fileDir = "E:\workspace\测试文档\集盒大学测试数据\课程评论"
    # re = readFile.getFileName(fileDir)
    #
    # for res in re:
    #     filePath = res
    #     fileName = os.path.basename(filePath).split(".")[0]
    #     print(fileName)
    #     re = readFile.readFile(filePath)
    #     print(len(re))
