import tkinter
from tkinter import ttk
from tkinter import filedialog
import os
import threading
from bilibPlayer.player.getProjectPath import getProjectPath


class wind(object):
    id = 0

    def __init__(self):
        self.win = tkinter.Tk()
        self.win.wm_grid(300, 400, 800, 500)
        self.win.title("播放器")
        self.window()
        self.win.mainloop()

    def window(self):
        frame1 = tkinter.Frame(self.win, bg="blue", width=200)
        frame1.pack(fill=tkinter.Y, side=tkinter.RIGHT, expand=0)

        frame2 = tkinter.Frame(self.win, bg="pink", width=600)
        frame2.pack(fill=tkinter.Y, side=tkinter.LEFT, expand=1)

        label = tkinter.Label(frame1, text="文件目录", width=28)
        label.pack(fill=tkinter.X)

        self.labe2 = tkinter.Label(frame1, text="文件区域", width=28)
        self.labe2.pack(fill=tkinter.BOTH)

        self.addMenu()
        self.mouseMenu()

    def addMenu(self):
        menubar = tkinter.Menu(self.win, tearoff=0)
        self.win.config(menu=menubar)
        menu1 = tkinter.Menu(tearoff=0)
        item = ["添加文件", "播放文件"]

        for it in item:
            if it == "添加文件":
                menu1.add_command(label=it, command=self.addFile)
            else:
                menu1.add_command(label=it, command=self.openFile)

        def start():
            print("播放")

        def stop():
            print("暂停")

        menubar.add_cascade(label="文件", menu=menu1)
        menubar.add_cascade(label="播放", command=start)
        menubar.add_cascade(label="暂停", command=stop)
        return menubar

    def mouseMenu(self):
        menubar = self.addMenu()

        def mouseClick(event):
            menubar.post(event.x_root, event.y_root)
        self.win.bind("<Button-3>", mouseClick)

    def addFile(self):
        file = filedialog.askdirectory()
        self.addListFile(file)
        # print("添加文件")

    def openFile(self):
        file = filedialog.askopenfilename()
        # print(file)
        self.playVideo(file)
        # print("打开文件")

    def playVideo(self, fileV , fileA=None):
        p = getProjectPath()
        ffplayPath = p + r"\resouce\ffplay.exe" + " " + "-window_title 播放器" + " " + "%s"
        ffplayPathA = p + r"\resouce\ffplay.exe" + " " + "-nodisp " + "%s"

        def playV():
            # print(ffplayPath)
            os.system(ffplayPath % fileV)
            os.system('taskkill /F /IM ffplay.exe')

        def playA():
            # print(ffplayPath)
            os.system(ffplayPathA % fileA)

        thV = threading.Thread(target=playV)
        thV.start()
        thA = threading.Thread(target=playA)
        thA.start()

    def addListFile(self, filePath):
        tree = ttk.Treeview(self.labe2, padding=tkinter.TOP)
        tree.pack()
        menu1 = tree.insert('', 0, "根目录", text=os.path.basename(filePath), values=[filePath])

        def lisdir(menu1, filePath):
            files = os.listdir(filePath)
            for file in files:
                newFilePath = os.path.join(filePath, file)
                menu2 = tree.insert(menu1, self.id, str(self.id), text=os.path.basename(newFilePath), values=[newFilePath])
                self.id += 1
                if os.path.isdir(newFilePath):
                     lisdir(menu2, newFilePath)

        lisdir(menu1, filePath)

        def play(event):
            ite = event.widget.selection()
            for it in ite:
                selectPath = tree.item(it)["values"][0]
                if os.path.split(selectPath)[1] == "video.m4s":
                    audioPath = os.path.split(selectPath)[0] + r"\audio.m4s"
                    # print(audioPath)
                    self.playVideo(selectPath, fileA=audioPath)
                else:
                    pass
                # print(tree.item(it)["values"][0])

        tree.bind("<<TreeviewSelect>>", play)


if __name__ == "__main__":
    w = wind()


