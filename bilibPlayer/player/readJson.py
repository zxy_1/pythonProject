import json
import os
from bilibPlayer.player.getProjectPath import getProjectPath


class OpenJson(object):

    def __init__(self):
        self.s = None

    def openJson(self, filePath):
        with open(filePath, "rb") as fp:
            st = fp.read()
            # s = dict(st)
            s = json.loads(st)
            # print(s["title"])
            # print(s['page_data']['part'])
            return s

    def openF(self, path):
        files = os.listdir(path)
        for fileName in files:
            filePath = os.path.join(path, fileName)
            if os.path.basename(filePath) == "entry.json":
                # print(filePath+"*****************")
                self.s = self.openJson(filePath)
                upPath = os.path.split(path)[0]
                os.rename(path, os.path.join(upPath, self.s['page_data']['part']))

            if os.path.basename(filePath) == "audio.m4s":
                p = getProjectPath()
                p1 = path + "\\audio.m4s"
                p2 = path + "\\video.m4s"
                p3 = p + "\\resouce\\ffmpeg.exe"
                p4 = path + "\\1.mp4"
                comm = "%s -i %s -i %s %s" % (p3, p1, p2, p4)
                # print(p1)
                # print(p2)
                # print(p3)
                # print(comm)
                os.system(comm)
            if os.path.isdir(filePath):
                self.openF(filePath)


# pf = r"F:\workspace\pythonProject\bilibPlayer\player\entry.json"
# path = r"F:\MyDownloads\Download\download\新建文件夹"
path = r"F:\MyDownloads\Download\download\测试\数据库测试"

P = OpenJson()
P.openF(path)
