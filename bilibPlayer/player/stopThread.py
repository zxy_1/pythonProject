import threading
import time
import inspect
import ctypes


def _async_raise(tid, exctype):
    """raises the exception, performs cleanup if needed"""
    tid = ctypes.c_long(tid)
    if not inspect.isclass(exctype):
        exctype = type(exctype)
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, ctypes.py_object(exctype))
    if res == 0:
        raise ValueError("invalid thread id")
    elif res != 1:
        # """if it returns a number greater than one, you're in trouble,
        # and you should call it again with exc=NULL to revert the effect"""
        ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, None)
        raise SystemError("PyThreadState_SetAsyncExc failed")


def stop_thread(thread):
    _async_raise(thread.ident, SystemExit)

def a():
    a = 0
    while a < 100:
        time.sleep(0.2)
        a += 1
        print("*********")

def b():
    while True:
        time.sleep(0.4)
        print("---------")

class TestThread(threading.Thread):
    def run(self):
        print("begin")
        while True:
            time.sleep(0.1)
        print("end")


if __name__ == "__main__":
    t = TestThread()
    t.start()
    time.sleep(1)
    stop_thread(t)

    t1 = threading.Thread(target=a)
    t1.start()
    t2 = threading.Thread(target=b)
    t2.start()
    time.sleep(1)
    while True:
        if t1.is_alive():
            pass
        else:
            stop_thread(t2)
            break
    print("stoped")