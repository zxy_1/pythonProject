import os


class FileOption(object):
    id = 0
    def addFile(self,path):
        if os.path.isdir(path):
            fileList = os.listdir(path)
            for file in fileList:
                filePath = os.path.join(path, file)
                print(filePath)
                if os.path.isdir(filePath):
                    self.addFile(filePath)
        else:
            print(path)

    def addFileToWin(self, tree, path):
        def p(tree1, path):
            fileList = os.listdir(path)
            for file in fileList:
                filePath = os.path.join(path, file)
                tree2 = tree.insert(tree1, 0, str(self.id), text=os.path.basename(filePath),values=[filePath])
                self.id += 1
                # print(filePath)
                if os.path.isdir(filePath):
                    p(tree2, filePath)

        if os.path.isdir(path):
            tree1 = tree.insert("", 0, "添加目录", text=os.path.basename(path), values=[path])
            p(tree1, path)


if __name__ == "__main__":
    path = r"F:\MyDownloads\Download\离线帮助文档"
    f = FileOption()
    f.addFile(path)