import sqlite3

import requests
import json
# base_url = "http://httpbin.org"
#
# request1 = requests.get(base_url+"/get")
# request2 = requests.post(base_url+'/post')
# print(request1.status_code)
# print(request2.status_code)

# base_url_param = "http://www.baidu.com/s"
# param = {"wd": "requests"}
# request3 = requests.get(base_url_param, params=param)
# print(request3.url)
# print(request3.status_code)
# print(request3.text)

# # get&post请求发送参数
# base_url = "http://httpbin.org"
# param={"name": "aba", "password": "12354"}
# request1 = requests.get(base_url+"/get", params=param)
# request2 = requests.post(base_url+'/post', data=param)
# print(request1.status_code)
# print(request1.text)
# print(request2.status_code)
# print(request2.text)

# # 请求头伪造
# base_url="http://httpbin.org"
#
# param_data = {"name": "abas", "password": "123455"}
# header = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0;Win64;x64) AppleWebKit/537.36(KHTML,like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763"}
# request2 = requests.post(base_url+'/post', data=param_data, headers=header)
#
# response = json.loads(request2.text)
# # print(request2.text)
# # print(request2.headers)
# print(response['headers']["User-Agent"])

db = sqlite3.connect("116.62.22.93:3306", "weixue", "wx_123456", "weixue")
cur = db.cursor()
cur.execute("select version()")
data = cur.fetchone()
print(data)
db.close()


