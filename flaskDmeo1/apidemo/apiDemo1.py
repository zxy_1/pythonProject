from flask import Flask, request, app
from flasgger import Swagger
from flask import render_template
from flaskDmeo1.apidemo.api import App


@app.route('/user', methods=['GET'])
def index(self):
    """
    获取用户信息接口
    接口参数：language ，size
    ---
    tags:
      - 这是我的pythonAPI文档
    parameters:
      - name: language
        in: query
        type: string
        required: true
        description: 用户语言
      - name: size
        in: query
        type: integer
        description: 尺寸大小
    responses:
      200:
        description: 访问成功！
    """
    if request.values.get("language") == "php":
        return "ok"
    # return "错误！"
    return render_template('index.html')