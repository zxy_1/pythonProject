from flask import Flask, request
from flaskDmeo1.apidemo.api import App


app = App()


@app.route('/v1/user/customer/code/bag/list', methods=['GET'])
def adduser():
    """
    获取用户信息接口
    接口参数：language ，size
    ---
    tags:
      - 这是我的pythonAPI文档
    parameters:
      - name: username
        in: query
        type: string
        required: true
        description: 用户名
      - name: pwd
        in: query
        type: string
        description: 密码
    responses:
      200:
        description: 访问成功！
    """
    # platform :android
    # _t : 1585314771921
    # token: 8UyaW4AIFcNcCeVh
    # status: 1
    # page: 1
    # is_page: 1
    # per_page: 10
    platform = request.values.get("platform")
    _t = request.values.get("_t")
    token = request.values.get("token")
    status = request.values.get("status")
    page = request.values.get("page")
    is_page = request.values.get("is_page")
    per_page = request.values.get("per_page")

    data = {
        "code": "200",
        "timestamp": "1585313807",
        "message": "success",
        "data": {
            "current_page": "1",
            "data": [
                {
                    "code_share_log_id": "126",
                    "code_id": "19869",
                    "status": "1",
                    "type": "2",
                    "code": "XYIWGiUzzFgU3fruKXdi",
                    "get_type": "1",
                    "valid_end_at": None
                }
            ],
            "first_page_url": "http://test2019.jiheapp.com/v1/user/customer/code/bag/list?page=1",
            "from": "1",
            "last_page": "1",
            "last_page_url": "http://test2019.jiheapp.com/v1/user/customer/code/bag/list?page=1",
            "next_page_url": None,
            "path": "http://test2019.jiheapp.com/v1/user/customer/code/bag/list",
            "per_page": "10",
            "prev_page_url": None,
            "to": "1",
            "total": "1"
        }
    }

    if _t is not None:
        return data
    else:
        return "erro!!"