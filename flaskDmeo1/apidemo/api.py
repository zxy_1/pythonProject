from flask import Flask
from flasgger import Swagger


class App:

    def __init__(self):
        self.app = Flask(__name__)
        Swagger(self.app)


