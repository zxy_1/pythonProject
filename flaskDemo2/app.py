from flask import Flask
from flask import make_response
from flaskDemo2.api.apiDemo1 import ApiDemo
from flaskDemo2.api.apiDemo2 import ApiDemo2

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


app.add_url_rule("/v1", endpoint="wx", view_func=ApiDemo2().test1, methods=['get'])
app.add_url_rule("/test", endpoint="name", view_func=ApiDemo().testApi, methods=['post'])


if __name__ == '__main__':
    app.run(debug=True, port=80, host='127.0.0.1')
