from flask import make_response


class ApiDemo2:
    # @app.route("/v1")
    def test1(self):
        date = {
            "code": "200",
            "timestamp": "1590051683",
            "message": "",
            "data": {
                "current_page": "1",
                "data": [{
                    "student_id": "12955",
                    "student_name": "tester100",
                    "avatar": "\/img\/20180824kj3ingiigak1cfdzqiwwt4xdavv7n595.jpg",
                    "enroll_num": "82",
                    "finish_num": "62"
                }, {
                    "student_id": "12950",
                    "student_name": "\u98d8\u6e3a",
                    "avatar": "\/img\/20180825\/5e0ce297fdcd82816f6712f4cf04f36b.jpg",
                    "enroll_num": "77",
                    "finish_num": "38"
                }, {
                    "student_id": "12971",
                    "student_name": "tester99",
                    "avatar": "",
                    "enroll_num": "48",
                    "finish_num": "23"
                }, {
                    "student_id": "12972",
                    "student_name": "tester98",
                    "avatar": "\/img\/2019-01-14_17:46:56:443.png",
                    "enroll_num": "39",
                    "finish_num": "20"
                }, {
                    "student_id": "13016",
                    "student_name": "\u6d4b\u8bd5\u4eba1",
                    "avatar": "",
                    "enroll_num": "37",
                    "finish_num": "18"
                }, {
                    "student_id": "13547",
                    "student_name": "test97",
                    "avatar": "\/img\/2019-01-14_17:35:05:792.png",
                    "enroll_num": "46",
                    "finish_num": "17"
                }, {
                    "student_id": "13427",
                    "student_name": "\u6d4b\u8bd533",
                    "avatar": "",
                    "enroll_num": "22",
                    "finish_num": "14"
                }, {
                    "student_id": "13052",
                    "student_name": "\u6d4b\u8bd501",
                    "avatar": "",
                    "enroll_num": "29",
                    "finish_num": "12"
                }, {
                    "student_id": "13286",
                    "student_name": "\u4ed8",
                    "avatar": "\/img\/20200225aiq8ylb0x4l28eydmbvamhi4twdewcdv.jpg",
                    "enroll_num": "28",
                    "finish_num": "10"
                }, {
                    "student_id": "12825",
                    "student_name": "\u738b\u738b\u738b",
                    "avatar": "\/img\/20180821\/9aff517e739964dcb6e704558a5ad942.JPG",
                    "enroll_num": "15",
                    "finish_num": "9"
                }],
                "first_page_url": "https:\/\/test.eweixue.com\/v1\/organization\/learning_rank?page=1",
                "from": "1",
                "last_page": "4",
                "last_page_url": "https:\/\/test.eweixue.com\/v1\/organization\/learning_rank?page=4",
                "next_page_url": "https:\/\/test.eweixue.com\/v1\/organization\/learning_rank?page=2",
                "path": "https:\/\/test.eweixue.com\/v1\/organization\/learning_rank",
                "per_page": "10",
                "prev_page_url": None,
                "to": "10",
                "total": "31"
            }
        }

        # 自行构建响应体
        resp = make_response(date)
        resp.headers["content-type"] = "txt/json"
        resp.headers["Access-Control-Allow-Origin"] = "*"

        return resp