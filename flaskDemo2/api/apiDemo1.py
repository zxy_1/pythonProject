from flask import request
from flask import make_response


class ApiDemo:
    def testApi(self):
        name = request.values.get("name")

        if name == "zhang":
            data = {"re": "python api"}
            resp = make_response(data)
            resp.headers["content-type"] = "txt/json"
            resp.headers["Access-Control-Allow-Origin"] = "*"

            return resp

        resp1 = make_response(name)
        resp1.headers["content-type"] = "txt/json"
        resp1.headers["Access-Control-Allow-Origin"] = "*"
        return resp1
