import smtplib
import os

from email.mime.text import MIMEText
from email.utils import formataddr
from email.mime.multipart import MIMEMultipart
from apiTestProject import readConfig
from apiTestProject import getpathInfo


class Mail(object):
    def __init__(self):
        read_conf = readConfig.ReadConfig()
        self.subject = read_conf.get_email('subject')  # 从配置文件中读取邮件主题（subject）
        self.sender = str(read_conf.get_email("sender"))  # 从配置文件中读取发件者地址
        self.pwd = str(read_conf.get_email('pwd'))  # 从配置文件中读取发件者的pwd
        self.address = str(read_conf.get_email('address'))  # 从配置文件中读取收件者（address）
        self.mail_path = os.path.join(getpathInfo.get_path(), 'result', 'report.html')  # 获取报告的地址

    def sendMail(self):
        ret = True
        try:
            msg = MIMEMultipart()
            # 创建邮件正文
            content = """
                       执行测试中……
                       测试已完成！！
                       生成报告中……
                       报告已生成……
                       报告已邮件发送！
                   """
            msg.attach(MIMEText(content, 'plain', 'utf-8'))

            # 构造附件,传送当前目录下的test.cvs
            att1 = MIMEText(open(self.mail_path, 'rb').read(), 'base64', 'utf-8')
            att1['Content-Type'] = 'application/octet-stream'
            # 这里的filename可以任意写，写什么名字，邮件中显示什么名字
            att1["Content-Disposition"] = 'attachment; filename="report.html"'
            msg.attach(att1)

            msg['From'] = formataddr(["朱星月", self.sender])  # 括号里的对应发件人邮箱昵称、发件人邮箱账号
            msg['To'] = formataddr(["蓝天白云", self.address])  # 括号里的对应收件人邮箱昵称、收件人邮箱账号
            msg['Subject'] = self.subject  # 邮件的主题，也可以说是标题
            server = smtplib.SMTP_SSL("smtp.qq.com", 465)  # 发件人邮箱中的SMTP服务器，端口是25
            server.login(self.sender, self.pwd)  # 括号中对应的是发件人邮箱账号、邮箱密码
            server.sendmail(self.sender, [self.address, ], msg.as_string())  # 括号中对应的是发件人邮箱账号、收件人邮箱账号、发送邮件
            server.quit()  # 关闭连接
        except Exception:  # 如果 try 中的语句没有执行，则会执行下面的 ret=False
            ret = False
        return ret


if __name__ == "__main__":
    mail = Mail()
    print(mail.subject)
    print(mail.sender)
    print(mail.pwd)
    print(mail.address)

    ret = mail.sendMail()
    if ret:
        print("邮件发送成功")
    else:
        print("邮件发送失败")


