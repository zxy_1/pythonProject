import requests   # 安装的三方库
import json


class RunMain(object):
    def send_post(self, url, data):
        # 规定参数必须按照url , data 顺序传入
        result = requests.post(url=url, data=data).json()
        res = json.dumps(result, ensure_ascii=False, sort_keys=True, indent=2)
        return res

    def send_get(self, url, data):
        result = requests.get(url=url, params=data).json()
        res = json.dumps(result, ensure_ascii=False, sort_keys=True, indent=2)
        return res

    # 定义一个run_main函数通过不同方法，发送不同请求
    def run_main(self, method, url=None, data=None):
        result = None
        if method == 'post':
            result = self.send_post(url, data)
        elif method == 'get':
            result = self.send_get(url, data)
        else:
            print("method值错误！！")
        return result


if __name__ == "__main__":
    # result = RunMain().run_main("post", "http://127.0.0.1:9811/login", 'username=zhuxingyue&pwd=111')
    result = RunMain().run_main("get", "http://test.eweixue.com/v1/course/broadcast?", {"course_id": "5458", "token": "GUar2rPTrEhcF3PF", "organization_id": "gO9X0G9z"})
    print(result)
    # http://test.eweixue.com/v1/course/broadcast
    # token
    # organization_id=gO9X0G9z
    # course_id















