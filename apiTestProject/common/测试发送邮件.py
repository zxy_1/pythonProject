'''
1.用win32模块中的outlook 发送邮件xvh
'''

# !/usr/bin/python
# -*- coding: UTF-8 -*-

import smtplib
from email.mime.text import MIMEText
from email.utils import formataddr
from email.mime.multipart import MIMEMultipart

my_sender = '235597034@qq.com'  # 发件人邮箱账号
my_pass = 'c'  # 发件人邮箱密码
my_user = '1156193618@qq.com'  # 收件人邮箱账号，我这边发送给自己


def mail():
    ret = True
    try:
        msg = MIMEMultipart()
        # 创建邮件正文
        msg.attach(MIMEText('测试已完成,发送测试报告请接收....', 'plain', 'utf-8'))

        # 构造附件,传送当前目录下的test.cvs
        att1 = MIMEText(open('test.csv', 'rb').read(), 'base64', 'utf-8')
        att1['Content-Type'] = 'application/octet-stream'
        # 这里的filename可以任意写，写什么名字，邮件中显示什么名字
        att1["Content-Disposition"] = 'attachment; filename="test.csv"'
        msg.attach(att1)

        msg['From'] = formataddr(["朱星月", my_sender])  # 括号里的对应发件人邮箱昵称、发件人邮箱账号
        msg['To'] = formataddr(["蓝天白云", my_user])  # 括号里的对应收件人邮箱昵称、收件人邮箱账号
        msg['Subject'] = "接口测试报告"  # 邮件的主题，也可以说是标题

        server = smtplib.SMTP_SSL("smtp.qq.com", 465)  # 发件人邮箱中的SMTP服务器，端口是25
        server.login(my_sender, my_pass)  # 括号中对应的是发件人邮箱账号、邮箱密码
        server.sendmail(my_sender, [my_user, ], msg.as_string())  # 括号中对应的是发件人邮箱账号、收件人邮箱账号、发送邮件
        server.quit()  # 关闭连接
    except Exception:  # 如果 try 中的语句没有执行，则会执行下面的 ret=False
        ret = False
    return ret


if __name__ == "__main__":
    ret = mail()
    if ret:
        print("邮件发送成功")
    else:
        print("邮件发送失败")