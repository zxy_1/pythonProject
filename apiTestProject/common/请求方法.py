import requests   # 安装的三方库
import json


class RunMain(object):
    def send_post(self, url, data):
        # 规定参数必须按照url , data 顺序传入
        result = requests.post(url=url, data=data).json()
        res = json.dumps(result, ensure_ascii=False, sort_keys=True, indent=2)
        return res

    def send_get(self, url, data):
        result = requests.get(url=url, params=data).json()
        res = json.dumps(result, ensure_ascii=False, sort_keys=True, indent=2)
        return res


if __name__ == "__main__":
    url = "http://test.eweixue.com/v1/course/broadcast"
    data = "course_id=5458&user_organization_id=gO9X0G9z&source=&version=4.6.7&platform=android&token=GUar2rPTrEhcF3PF"

    url1 = "https://test.eweixue.com/v1/course/live/send"
    data1 = {"meta":{"comment_type":0,"course_id":"5458","ext":{"bindImageId":2863,"duration":"3","role":"3","src":"/img/201907131emm9wyd5a7qkyek7akoocs30y6rv7xw.mp3"},"from_user":{"avatar":"/img/20180825/5e0ce297fdcd82816f6712f4cf04f36b.jpg","nickname":"飘邈"},"group_id":"2367","type":2},"is_organization":"0","platform":"android","token":"GUar2rPTrEhcF3PF","isChecked":'false'}

    req = RunMain()
    s = req.send_get(url, data)
    s1 = req.send_post(url1, data1)
    print(s)
    print(s1)


