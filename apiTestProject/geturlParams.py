import readConfig

readConfig = readConfig.ReadConfig()   # 获取读取文件的配置（自定义的模块）


class GeturlParams(object):
    def get_url(self):
        new_url = readConfig.get_http('scheme') + '://' + readConfig.get_http('baseurl') + ':9811' + '/login' + '?'
        return new_url

    # 新的配置
    def get_url1(self, requestPath):
        new_url = readConfig.get_http('scheme') + '://' + readConfig.get_http('baseurl') + readConfig.get_http('port') + requestPath + '?'
        return new_url


if __name__ == "__main__":
    print(GeturlParams().get_url())



