import json
import unittest
from common.configHttp import RunMain
import paramunittest   # 安装的三方库
import geturlParams
import urllib.parse
import readExcel

url = geturlParams.GeturlParams().get_url()   # 调用创建的读取配置文件中的url
login_xls = readExcel.ReadExcel().get_xls('userCase.xlsx', 'Sheet1')


@paramunittest.parametrized(*login_xls)
class TestUserLogin(unittest.TestCase):
    # 设置excel文件对应字段的参数
    def setParameters(self, case_name, path, query, method):
        self.case_name = str(case_name)
        self.path = str(path)
        self.query = str(query)
        self.method = str(method)

    def description(self):
        self.case_name

    # 断言函数
    def checkResult(self):
        url1 = "http://www.xxx.com/login?"
        new_url = url1 + self.query

        # 将一个完整的url中的参数name=&pwd=转换为{'name':'xxx','pwd':'bbb'}
        data1 = dict(urllib.parse.parse_qsl(urllib.parse.urlsplit(new_url).query))
        info = RunMain().run_main(self.method, url, data1)
        ss = json.loads(info)

        if self.case_name == 'login':
            self.assertEqual(ss['code'], 200)
        if self.case_name == 'login_error':
            self.assertEqual(ss['code'], -1)
        if self.case_name == 'login_null':
            self.assertEqual(ss['code'], 10001)

    def setUp(self):
        print(self.case_name+"测试开始准备")

    def test01case(self):
        self.checkResult()

    def tearDown(self):
        print("测试结束，输入log 完结\n\n")




