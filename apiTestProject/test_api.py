import flask
import json
from flask import request

'''
创建本地接口调试服务调试代码：
flask:web框架，通过flask提供的装饰器@sever.route()将普通的函数转化为服务端的路由接口
    
'''
server = flask.Flask(__name__)


@server.route('/login', methods=['get', 'post'])
def login():

    username = request.values.get('username')

    pwd = request.values.get("pwd")

    if username and pwd:
        if username == 'zhuxingyue' and pwd == '111':
            resu = {'code': 200, 'message': "登陆成功"}
            return json.dumps(resu, ensure_ascii=False)
        else:
            resu = {'code': -1, 'message': "账号密码错误"}
            return json.dumps(resu, ensure_ascii=False)
    else:
        resu = {'code': 10001, 'message': "参数不能为空"}
        return json.dumps(resu, ensure_ascii=False)


if __name__ == "__main__":
    server.run(debug=True, port=9811, host='127.0.0.1')











