import os
import getpathInfo   # 调用自定义的文件获取绝对路径
# 导入第三方读取excel的库
from xlrd import open_workbook

path = getpathInfo.get_path()


class ReadExcel(object):

    def get_xls(self, xls_name, sheet_name):
        cls = []
        xlsPath = os.path.join(path, 'testFile', 'case', xls_name)
        file = open_workbook(xlsPath)   # 打开excel文件
        sheet = file.sheet_by_name(sheet_name)  # 打开excel中对应的sheet
        nrows = sheet.nrows   # 获取sheet中的行数
        for i in range(nrows):
            if sheet.row_values(i)[0] != u'case_name':
                cls.append(sheet.row_values(i))
        return cls

    # 新的配置
    def getpath(self, xls_name, sheet_name):
        cls = ""
        xlsPath = os.path.join(path, 'testFile', 'case', xls_name)
        file = open_workbook(xlsPath)   # 打开excel文件
        sheet = file.sheet_by_name(sheet_name)  # 打开excel中对应的sheet
        nrows = sheet.nrows   # 获取sheet中的行数
        for i in range(nrows):
            if sheet.row_values(i)[0] != u'case_name':
                if sheet.row_values(i)[1] == sheet.row_values(i-1)[1]:
                    cls = sheet.row_values(2)[1]
        return cls


if __name__ == "__main__":
    print(ReadExcel().get_xls("userCase.xlsx", 'Sheet1'))
    print(ReadExcel().get_xls("userCase.xlsx", 'Sheet1')[0][0])
    print(ReadExcel().get_xls("userCase.xlsx", 'Sheet1')[1])
    print(ReadExcel().get_xls("userCase.xlsx", 'Sheet1')[2])





