import os
import configparser
from apiTestProject import getpathInfo

'''
    configparser 模块是用来读取外部文件
'''

path = getpathInfo.get_path()   # 实例化调用查看当前绝对路径
config_path = os.path.join(path, 'config.ini')  # F:\workspace\pythonProject\apiTestProject\config.ini
config = configparser.ConfigParser()  # 调用外部的读取文件的方法
config.read(config_path, encoding="utf-8")


class ReadConfig(object):
    def get_http(self, name):
        value = config.get('HTTP', name)
        return value

    def get_email(self, name):
        value = config.get('EMAIL', name)
        return value

    def get_mysql(self, name):
        value = config.get('MYSQL', name)
        return value


if __name__ == "__main__":
    print("HTTP中的baseurl值为：", ReadConfig().get_http("baseurl"))
    print("EMAIL中的address为：", ReadConfig().get_email("address"))





