"""mock URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from rest_framework import routers

from web import views
from web.tests import *

from rest_framework.schemas import get_schema_view
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer
schema_view = get_schema_view(title='API', renderer_classes=[OpenAPIRenderer, SwaggerUIRenderer])


# 路由
# router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet, base_name='user')
# router.register(r'groups', views.GroupViewSet, base_name='group')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', login),
    path('infor/', infor),
    # drf登录
    path(r'api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('docs/', schema_view, name='docs'),  # 线上环境中，最好去掉
]
