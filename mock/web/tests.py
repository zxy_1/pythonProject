from django.test import TestCase
from django.http.response import HttpResponse
from django.shortcuts import render_to_response
import json
# Create your tests here.


def login(request):
    if request.method == "POST":
        username = request.POST.get("username")
        return HttpResponse(username)
    else:
        return render_to_response("hello.html")


def infor(request):
    jso = {
      "data": {
        "name": "zhangsan",
        "sex": "man",
        "age": 20
      }
    }
    if request.method == "GET":
        username = request.GET.get("username")
        if username == "zhang":
            res = json.dumps(jso)
            return HttpResponse(res, content_type="application/json", charset="utf-8")
        else:
            return render_to_response("hello.html")
    else:
        return render_to_response("hello.html")
