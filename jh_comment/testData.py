from tools.readFile import ReadFile
from tools.sqlRead import DBconnect
import os
from apiTest.login import Login
from apiTest.comment import Comment
import time
from dataTimer import DataTimer
import threading
from apiTest.updateUser import UpdateUser
import random


class TestDmeo():
    # 评论文件地址
    fileDir = "E:\workspace\测试文档\集盒大学测试数据\课程评论"

    # 用户昵称地址
    nameFilePath = "E:\workspace\测试文档\集盒大学测试数据\昵称.txt"

    # 获取课程名称,查询课程id
    def getCourseName(self):
        courseInfo = {}

        readFile = ReadFile()
        db = DBconnect()

        fileNamePaths = readFile.getFileName(self.fileDir)

        for fileNamePath in fileNamePaths:
            fileName = os.path.basename(fileNamePath).split(".")[0]

            sql = "select * from jh_course where `status`='1' and title = '" + fileName + "'"

            # 连接数据查询课程id，并返回id列表
            dbc = db.dbConn()
            idList = db.search(dbc, sql)
            db.dbClose(dbc)

            courseInfo.setdefault(fileName, idList[0])
        return courseInfo

    # 获取课程对应的评论
    def getCourseComment(self):
        courseCommentInfo = {}
        readFile = ReadFile()

        fileNamePaths = readFile.getFileName(self.fileDir)
        for fileNamePath in fileNamePaths:
            fileName = os.path.basename(fileNamePath).split(".")[0]
            comList = readFile.readFile(fileNamePath)
            courseCommentInfo.setdefault(fileName, comList)
        return courseCommentInfo

    # 获取用户电话号码，登陆后获取token
    def getToken(self):
        tokenList = []
        sql = "select mobile from jh_customer_login where mobile like '1521234%'"
        db = DBconnect()
        dbc = db.dbConn()
        mobileList = db.search(dbc, sql)
        db.dbClose(dbc)
        for mobile in mobileList:
            login = Login(mobile=mobile)
            token = login.login()
            tokenList.append(token)
        return tokenList

    # 获取用户名称,头像；保存字典中
    def getUserName(self):
        userInfor = {}

        # 获取昵称
        readFile = ReadFile()
        userNameList = readFile.readFile(self.nameFilePath)

        # 获取头像
        sql = "select ppt from jh_course_ppt where course_id = '46'"
        db = DBconnect()
        dbc = db.dbConn()
        imageList = db.search(dbc, sql)
        db.dbClose(dbc)

        for index in range(len(imageList)):
            userInfor.setdefault(userNameList[index], imageList[index])
        return userInfor


if __name__ == "__main__":
    # 日志文件地址
    logPath = "E:\workspace\测试文档\集盒大学测试数据\log.txt"

    test = TestDmeo()
    readFile = ReadFile()

    # 获取课程id
    courseInfo = test.getCourseName()
    courseNames = courseInfo.keys()
    courseNameList = list(courseNames)

    # 获取课程评论
    courseCommentInfo = test.getCourseComment()

    # 获取token
    tokenList = test.getToken()

    # 获取昵称，头像
    userInfo = test.getUserName()

    # 更新用户昵称，头像
    def upUser(tokenList, userInfo):
        namelist = list(userInfo.keys())

        index = 0
        while(index < len(tokenList)):
            updateUser = UpdateUser(tokenList[index])
            updateUser.updateUserName(namelist[index])
            updateUser.updateUserHeadImage(userInfo.get(namelist[index]))
            print(tokenList[index])
            print(namelist[index])
            print(userInfo.get(namelist[index]))
            index += 1

            # 记录日志
            readFile.writeFile(logPath, "【%s】用户信息更新完成！！！！" % (DataTimer().getTime(),))


    # 开启多线程，更新用户头像，昵称
    t1 = threading.Thread(target=upUser, args=(tokenList, userInfo))
    t1.start()

    tokenIndex = 0
    courseIndex = 0
    commentIndex = 0

    while(courseIndex < len(courseNameList)):

        # 定时执行任务
        timer = DataTimer()
        datetime = timer.timer()
        if datetime is False:
            continue

        comments = courseCommentInfo.get(courseNameList[courseIndex])
        courseIndex += 1

        # 每个课程的评论是否结束，结束后获取下一个课程的评论
        if commentIndex >= len(comments):
            continue

        # print("token: %s" % tokenList[tokenIndex])
        # print(str(courseNameList[courseIndex-1]) + ":" + str(courseInfo.get(courseNameList[courseIndex-1])))
        # print(comments[commentIndex])

        # 提交评论
        com = Comment(course_id=courseInfo.get(courseNameList[courseIndex-1]), token=tokenList[tokenIndex])
        com.userComment(message=comments[commentIndex])

        # 记录日志文件
        readFile.writeFile(logPath, "【%s】提交一条评论你成功！！！ " % (timer.getTime(),))

        # 评论间隔随机6~9分钟
        a = random.randint(7, 10)
        time.sleep(a*60)

        tokenIndex += 1

        # 判断用户用户是否用完，用完后轮回使用
        if tokenIndex >= len(tokenList):
            tokenIndex = 0

        # 判断课程循环，如果课程循环结束；自动获取每个课程的第二跳评论
        if courseIndex == len(courseNameList):
            commentIndex += 1
            courseIndex = 0







