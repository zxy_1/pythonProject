import time


class DataTimer():

    # 获取当前时间字符串
    def getTime(self):

        # 格式化成2016-03-20 11:45:39形式
        nametime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        return nametime

    # 判断时间段
    def selectTime(self, start, end):

        # 获取当前时间戳
        datime = int(time.time())

        # 将时间字符串转换成时间戳
        beforeTime = int(time.mktime(time.strptime(start, "%Y-%m-%d %H:%M:%S")))
        afterTime = int(time.mktime(time.strptime(end, "%Y-%m-%d %H:%M:%S")))

        if datime not in range(beforeTime, afterTime):
            return False
        else:
            return True

    # 设置时间段
    def timer(self):

        # 判断时间是否在规定时间内
        a = "2020-01-24 09:00:00"
        b = "2020-01-24 21:59:00"

        a1 = "2020-01-20 09:00:00"
        b1 = "2020-01-20 21:59:00"

        a2 = "2020-01-21 09:00:00"
        b2 = "2020-01-21 21:59:00"

        a3 = "2020-01-22 09:00:00"
        b3 = "2020-01-22 21:59:00"

        a4 = "2020-01-23 09:00:00"
        b4 = "2020-01-23 21:59:00"

        date1 = self.selectTime(a, b)
        date2 = self.selectTime(a1, b1)
        date3 = self.selectTime(a2, b2)
        date4 = self.selectTime(a3, b3)
        date5 = self.selectTime(a4, b4)

        if date1 or date2 or date3 or date4 or date5:
            return True
        else:
            return False


# if __name__ == "__main__":
#     timer = DataTimer()
#     a = timer.timer()
#     print(a)
