# -*- encoding=utf8 -*-driver.get("Write your test web address!")
"""
    链接设备的工具类
"""

__author__ = "admin"
from airtest.core.api import *
from airtest_selenium.proxy import WebChrome


def connectAndroid():
    """链接Android设备"""
    try:
        init_device(platform='Android', uuid='N8K7N16B14011457')
    except Exception:
        print("设备的id错误！！")
    start_app("com.guanghua.jiheuniversity")



def connectBrowser():
    """链接chrome浏览器"""
    driver = WebChrome()
    driver.implicitly_wait(20)
    return driver


def backPage():
    """页面返回"""
    keyevent("BACK")


def pageSleep():
    """强制延迟0.5s"""
    sleep(0.5)