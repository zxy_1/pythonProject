"""
    截取屏幕保存图片
"""
from airtest.core.api import *

from airtestDemo.commonTools.devicesConnect import connectAndroid


def imgSave(imgName):
    """截屏保存图片"""

    # 保存图片相对路径根据运行代码的相对路径
    imgPath = "./image/reportImage"

    snapshot(('{}/{}.png'.format(os.path.abspath(imgPath), imgName)))
    print(('{}/{}.png'.format(os.path.abspath(imgPath), imgName)))


if __name__ == "__main__":
    connectAndroid()
    imgSave("test")