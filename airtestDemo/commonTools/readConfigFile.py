"""
    读取配置文件的工具类
"""
import configparser

# 配置文件地址
configFilePath = r"F:\workspace\pythonProject\airtestDemo\.editorconfigqwer"

config = configparser.ConfigParser()
config.read(configFilePath, encoding='utf-8')


class ReadConfigFile:

    @staticmethod
    def getEmail(key):
        """获取email信息"""
        return config.get("email", key)


if __name__ == "__main__":
    readConfigFile = ReadConfigFile()
    name = readConfigFile.getEmail("name")
    print(name)