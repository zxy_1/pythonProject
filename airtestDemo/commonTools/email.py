"""
    配置发送邮件的工具类
"""

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formataddr

from airtestDemo.commonTools.readConfigFile import ReadConfigFile


class Email:

    readConfigFile = ReadConfigFile()
    emailSwich = readConfigFile.getEmail("on_off")
    subject = readConfigFile.getEmail("subject")
    sender = readConfigFile.getEmail("sender")
    pwd = readConfigFile.getEmail("pwd")
    address = readConfigFile.getEmail("address").split(",")
    # mail_path = r"F:\workspace\pythonProject\airtestDemo\report\beautifulReport\report.html"

    def mail(self, reportPath):
        """配置发送邮件
        :param reportPath: 测试报告文件地址
        :return ret: 返回Boolean类型发送成功True
        """

        ret = True
        try:
            msg = MIMEMultipart()
            # 创建邮件正文
            content = """
                                  执行测试中……
                                  测试已完成！！
                                  生成报告中……
                                  报告已生成……
                                  报告已邮件发送！
                              """
            msg.attach(MIMEText(content, 'plain', 'utf-8'))

            # 构造附件,传送当前目录下的test.cvs
            att1 = MIMEText(open(reportPath, 'rb').read(), 'base64', 'utf-8')
            att1['Content-Type'] = 'application/octet-stream'
            # 这里的filename可以任意写，写什么名字，邮件中显示什么名字(注意不能试用中文)
            att1["Content-Disposition"] = 'attachment; filename="jh_report.html"'
            msg.attach(att1)

            for address in self.address:
                msg['From'] = formataddr(["朱星月", self.sender])  # 括号里的对应发件人邮箱昵称、发件人邮箱账号
                msg['To'] = formataddr(["蓝天白云", address])  # 括号里的对应收件人邮箱昵称、收件人邮箱账号
                msg['Subject'] = self.subject  # 邮件的主题，也可以说是标题

            server = smtplib.SMTP_SSL("smtp.qq.com", 465)  # 发件人邮箱中的SMTP服务器，端口是25
            server.login(self.sender, self.pwd)  # 括号中对应的是发件人邮箱账号、邮箱密码

            server.sendmail(self.sender, self.address, msg.as_string())  # 括号中对应的是发件人邮箱账号、收件人邮箱账号、发送邮件

            server.quit()  # 关闭连接
        except Exception:
            ret = False

        return ret

    def sendMail(self, reportPath):
        """发送邮件
        :param reportPath: 测试报告地址
        """
        if self.mail(reportPath):
            print("邮件已发送成功！！")
        else:
            print("邮件发送失败！！")


if __name__ == "__main__":
    email = Email()
    print(email.subject)
    print(email.address[1])

    if email.sendMail():
        print("发送成功！")
    else:
        print("发送失败！")