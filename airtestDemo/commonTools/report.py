# -*- encoding=utf8 -*-
__author__ = "admin"
import unittest

from airtest.core.api import *
from airtestDemo.testDemo import testwithdrawdeposit, testWithdrawFlow
from airtest.cli.parser import cli_setup
from airtest.report.report import simple_report

if not cli_setup():
    auto_setup(__file__, logdir="../report/airtestReport/log", devices=[
        "android:///",
    ])


# script content
print("start...")

suite = unittest.TestSuite()
loader = unittest.TestLoader()
suite.addTests(loader.loadTestsFromModule(testWithdrawFlow))
suite.addTests(loader.loadTestsFromModule(testwithdrawdeposit))
runner = unittest.TextTestRunner()
runner.run(suite)

simple_report(__file__, logpath="../report/airtestReport/log", output="../report/airtestReport/airtestReport.html")