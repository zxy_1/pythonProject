import unittest, time, os

from BeautifulReport import BeautifulReport

from airtestDemo.commonTools.email import Email
from airtestDemo.testDemo import testwithdrawdeposit, testWithdrawFlow

currentPath = os.getcwd()
reportPath = os.path.join(currentPath, "report/beautifulReport/")


now = time.strftime("%Y-%m-%d %H-%M-%S", time.localtime(time.time()))
# reportTitle = 'Example报告' + now + ".html"
reportTitle = 'Example报告'


if __name__ == "__main__":
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    suite.addTests(loader.loadTestsFromModule(testWithdrawFlow))
    suite.addTests(loader.loadTestsFromModule(testwithdrawdeposit))
    BeautifulReport(suite).report(filename=reportTitle, description='提现页面测试', log_path=reportPath)

    email = Email()
    if email.emailSwich == "off":
        print("发送邮件功能关闭！！")
    else:
        email.sendMail(reportPath+reportTitle+".html")