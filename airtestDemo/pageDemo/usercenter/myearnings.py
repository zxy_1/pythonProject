"""
我的收益页面
"""
from airtestDemo.pageDemo.basepage import BasePage
from airtestDemo.pageDemo.usercenter.withdrawdeposit import WithdrawDeposit


class MyEarnings(BasePage):
    # 可支出金额的id
    usableExpend = "com.guanghua.jiheuniversity:id/withdraw_fee"

    # 提现按钮id
    withdrawButton = "com.guanghua.jiheuniversity:id/with_draw"

    def getUsableExpend(self):
        """返回可支出金额"""
        return self.getText(self.usableExpend)

    def withdrawButtonClick(self):
        """点击提现按钮，进入提现页面"""
        self.elementClick(self.withdrawButton)
        return WithdrawDeposit()
