"""
个人中心页面
"""
from airtestDemo.pageDemo.basepage import BasePage
from airtestDemo.pageDemo.usercenter.myearnings import MyEarnings
from airtestDemo.pageDemo.usercenter.tearcherCenter.tearchCenter import TeacherCenter


class UserCenter(BasePage):
    # 讲师中心定位方法
    tearcherCenter = ("text", "讲师中心")

    # 我的收益id
    myEarnings = "com.guanghua.jiheuniversity:id/tv_option"


    def tearcherCenterClick(self):
        """点击讲师中心跳转"""
        self.elementClick(self.tearcherCenter)
        return TeacherCenter()

    def myEarningsClick(self):
        """点击我的收益跳转"""
        self.elementSwipe("U")
        self.elementClick(self.myEarnings)
        return MyEarnings()