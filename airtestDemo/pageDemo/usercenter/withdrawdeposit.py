"""
提现页面
"""
from airtestDemo.pageDemo.basepage import BasePage


class WithdrawDeposit(BasePage):
    # 可提现金额id
    canWithdraw = "com.guanghua.jiheuniversity:id/tv_can_withdraw"

    # 全部提现按钮id
    allWithdraw = "com.guanghua.jiheuniversity:id/tv_withdraw_all"

    # 提现金额输入框地址
    withdrawExpend = "com.guanghua.jiheuniversity:id/comment_area"

    # 选择微信提现按钮id
    withdrawWechat = "com.guanghua.jiheuniversity:id/iv_wechat"

    # 选择支付宝提现按钮id
    withdrawAlipay = "com.guanghua.jiheuniversity:id/iv_alipay"

    # 支付宝账号输入框地址
    alipayAccount = "com.guanghua.jiheuniversity:id/et_alipay_account"

    # 收款方姓名输入框地址
    alipayName = "com.guanghua.jiheuniversity:id/et_alipay_name"

    # 微信收款方姓名输入框
    wechatName = "com.guanghua.jiheuniversity:id/et_wechat_name"

    # 获取验证码
    getCode = "com.guanghua.jiheuniversity:id/get_valid_code"

    # 验证码输入框
    inputCode = "com.guanghua.jiheuniversity:id/input_password"

    # 确认按钮输入框
    buttonExchange = "com.guanghua.jiheuniversity:id/btn_exchange"


    def getWithdraw(self):
        """返回可提现金额"""
        return self.getText(self.canWithdraw)

    def allWithdrawClick(self):
        """点击全部提现按钮"""
        self.elementClick(self.allWithdraw)

    def inputWithdrawExpend(self, text):
        """输入提现金额"""
        self.elementClick(self.withdrawExpend)
        self.setText(self.withdrawExpend, text)

    def selectAlipayClick(self):
        """选择支付宝提现"""
        self.elementClick(self.withdrawAlipay)

    def selectWechatClick(self):
        """选择微信提现"""
        self.elementClick(self.withdrawWechat)

    def inputAlipayAccount(self, text):
        """输入支付宝账号"""
        self.elementClick(self.alipayAccount)
        self.setText(self.alipayAccount, text)

    def inputAlipayName(self, text):
        """输入支付宝收款方姓名"""
        self.elementClick(self.alipayName)
        self.setText(self.alipayName, text)

    def inputWechatName(self, text):
        """输入微信收款方姓名"""
        self.elementClick(self.wechatName)
        self.setText(self.wechatName, text)

    def getCodeClick(self):
        """点击获取验证码"""
        self.elementClick(self.getCode)

    def inputSecurityCode(self, text):
        """输入验证码"""
        self.elementClick(self.inputCode)
        self.setText(self.inputCode, text)

    def buttonExchangeClick(self):
        """点击立即体现按钮"""
        self.elementClick(self.buttonExchange)