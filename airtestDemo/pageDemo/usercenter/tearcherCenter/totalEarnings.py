"""
讲师中心，累计收益页面
"""
from airtestDemo.pageDemo.basepage import BasePage
from airtestDemo.pageDemo.usercenter.withdrawdeposit import WithdrawDeposit


class TotalEarnings(BasePage):

    # 可提现金额id
    canWithdraw = "com.guanghua.jiheuniversity:id/freeze_fee"

    def getCanWithdraw(self):
        """返回可提现金额"""
        return self.getText(self.canWithdraw)

    def canWithdrawClick(self):
        """点击可提现跳转体现页面"""
        self.elementClick(self.canWithdraw)
        return WithdrawDeposit()
