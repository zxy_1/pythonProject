"""讲师中心页面"""
from airtestDemo.pageDemo.basepage import BasePage
from airtestDemo.pageDemo.usercenter.tearcherCenter.totalEarnings import TotalEarnings


class TeacherCenter(BasePage):

    # 累计收益id
    totalEarnings = "com.guanghua.jiheuniversity:id/tv_all_money"

    def totalEarningsClick(self):
        """点击累计收益跳转累计收益页面"""
        self.elementClick(self.totalEarnings)
        return TotalEarnings()