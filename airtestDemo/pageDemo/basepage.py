"""
基础页面
"""
from airtest.core.api import *
from poco.drivers.android.uiautomation import AndroidUiautomationPoco


class BasePage:
    poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)

    def getText(self, elementAddress):
        """获取元素的文字"""
        return self.poco(elementAddress).get_text()

    def setText(self, elementAddress, vartext):
        """元素输入文字"""
        self.poco(elementAddress).set_text(vartext)


    def elementClick(self, elementAddress):
        """定位元素点击操作"""
        if type(elementAddress) is str:
            self.poco(elementAddress).click()
        else:
            self.poco(text=elementAddress[1]).click()

    def elementSwipe(self, duration):
        """页面滑动"""
        if duration == "U":
            swipe((550, 1500), (550, 600), duration=0.5)
        elif duration == "D":
            swipe((550, 600), (550, 1500), duration=0.5)
        elif duration == "L":
            swipe((800, 1000), (300, 1000), duration=0.5)
        elif duration == "R":
            swipe((300, 1000), (800, 1000), duration=0.5)
        else:
            print("输入页面滑动方向错误！！")