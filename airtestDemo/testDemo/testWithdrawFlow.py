"""
测试支付宝提现流程是否正确
"""
import unittest

from airtestDemo.commonTools.devicesConnect import connectAndroid, backPage, pageSleep
from airtestDemo.pageDemo.usercenter.usercenter import UserCenter
from airtestDemo.commonTools.beautifulReportShort import imgSave


class TesWithdrawFlow(unittest.TestCase):
    userCenter = UserCenter()

    def setUp(self) -> None:
        connectAndroid()

    def tearDown(self) -> None:
        pass

    def testTeacherWithdrawFlow(self):
        """测试讲师支付宝提现流程"""

        teacherCenter = self.userCenter.tearcherCenterClick()
        teacherTotalEarnings = teacherCenter.totalEarningsClick()
        withdrawDeposit = teacherTotalEarnings.canWithdrawClick()

        withdrawDeposit.inputWithdrawExpend("10")
        withdrawDeposit.inputAlipayAccount("15212340300")
        withdrawDeposit.inputAlipayName("测试")
        withdrawDeposit.getCodeClick()
        withdrawDeposit.inputSecurityCode("1234")
        withdrawDeposit.buttonExchangeClick()
        imgSave("testTeacherWithdrawFlow")

        for i in range(3):
            backPage()
        print("测试完成！")

    def testMyWithdrawFlow(self):
        """测试我的收益支付宝提现流程"""

        myEarnings = self.userCenter.myEarningsClick()
        withdrawDeposit = myEarnings.withdrawButtonClick()

        withdrawDeposit.inputWithdrawExpend("10")
        withdrawDeposit.inputAlipayAccount("15212340300")
        withdrawDeposit.inputAlipayName("测试")
        withdrawDeposit.getCodeClick()
        withdrawDeposit.inputSecurityCode("1234")
        withdrawDeposit.buttonExchangeClick()
        imgSave("testMyWithdrawFlow")

        for i in range(2):
            backPage()
        pageSleep()
        self.userCenter.elementSwipe("D")
        print("测试完成！")

    def testTeacherWithdrawWXFlow(self):
        """测试讲师微信提现流程"""

        teacherCenter = self.userCenter.tearcherCenterClick()
        teacherTotalEarnings = teacherCenter.totalEarningsClick()
        withdrawDeposit = teacherTotalEarnings.canWithdrawClick()

        withdrawDeposit.inputWithdrawExpend("10")
        withdrawDeposit.selectWechatClick()
        withdrawDeposit.inputWechatName("测试")
        withdrawDeposit.getCodeClick()
        withdrawDeposit.inputSecurityCode("1234")
        withdrawDeposit.buttonExchangeClick()
        imgSave("testTeacherWithdrawWXFlow")

        for i in range(3):
            backPage()
        print("测试完成！")

    def testMyWithdrawWXFlow(self):
        """测试我的收益微信提现流程"""

        myEarnings = self.userCenter.myEarningsClick()
        withdrawDeposit = myEarnings.withdrawButtonClick()

        withdrawDeposit.inputWithdrawExpend("10")
        withdrawDeposit.selectWechatClick()
        withdrawDeposit.inputWechatName("测试")
        withdrawDeposit.getCodeClick()
        withdrawDeposit.inputSecurityCode("1234")
        withdrawDeposit.buttonExchangeClick()
        imgSave("testMyWithdrawWXFlow")

        for i in range(2):
            backPage()
        pageSleep()
        self.userCenter.elementSwipe("D")

        print("测试完成！")


if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(TesWithdrawFlow("testTeacherWithdrawFlow"))
    suite.addTest(TesWithdrawFlow("testMyWithdrawFlow"))
    suite.addTest(TesWithdrawFlow("testTeacherWithdrawWXFlow"))
    suite.addTest(TesWithdrawFlow("testMyWithdrawWXFlow"))
    runner = unittest.TextTestRunner()
    runner.run(suite)
