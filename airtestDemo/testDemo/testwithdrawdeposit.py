"""
测试提现页面可提现金额显示是否正确
"""
import unittest

from airtestDemo.commonTools.beautifulReportShort import imgSave
from airtestDemo.commonTools.devicesConnect import connectAndroid, backPage, pageSleep
from airtestDemo.pageDemo.usercenter.usercenter import UserCenter


class TesWithdrawDeposit(unittest.TestCase):
    userCenter = UserCenter()

    def setUp(self) -> None:
        connectAndroid()

    def tearDown(self) -> None:
        pass

    def testTeacherWithdrawDisplay(self):
        """测试讲师收益可提现金额显示是否正确"""

        teacherCenter = self.userCenter.tearcherCenterClick()
        teacherTotalEarnings = teacherCenter.totalEarningsClick()

        canWithdrawMoney = teacherTotalEarnings.getCanWithdraw()
        withdrawDeposit = teacherTotalEarnings.canWithdrawClick()
        withdrawMoney = withdrawDeposit.getWithdraw()

        print("讲师累计收益可提现金额："+canWithdrawMoney)
        print("讲师收益提现页面可提现金额："+withdrawMoney)
        assert withdrawMoney in canWithdrawMoney
        imgSave("testTeacherWithdrawDisplay")

        for i in range(4):
            backPage()
        print("测试完成！")

    def testMyWithdrawDisplay(self):
        """测试我的收益可提现金额显示是否正确"""

        myEarnings = self.userCenter.myEarningsClick()

        expendMoney = myEarnings.getUsableExpend()
        withdrawDeposit = myEarnings.withdrawButtonClick()
        withdrawMoney = withdrawDeposit.getWithdraw()

        print("我的收益可支出金额："+expendMoney)
        print("我的收益体现页面可提现金额："+withdrawMoney)
        assert withdrawMoney in expendMoney
        imgSave("testMyWithdrawDisplay")


        for i in range(3):
            backPage()
        pageSleep()
        self.userCenter.elementSwipe("D")
        print("测试完成！")


if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(TesWithdrawDeposit("testTeacherWithdrawDisplay"))
    suite.addTest(TesWithdrawDeposit("testMyWithdrawDisplay"))
    runner = unittest.TextTestRunner()
    runner.run(suite)