import requests
import json
# https://www.teambition.com/api/projects/5e95a799b8ddcd00222417bc/smartgroups/5e95a799b8ddcd00222417bf/tasks?
# filter=_commongroupId%3Dnull%20AND%20isDone%3Dtrue%20AND%20taskLayer%20IN%20%5B0%2C1%2C2%2C3%2C4%2C5%2C6%2C7%2C8%5D%20AND%20_appId%3D5e95a799b8ddcd00222417bf%20AND%20taskType%20%3D%20bug&_=1588062677081
url = "https://www.teambition.com/api/projects/5e95a799b8ddcd00222417bc/smartgroups/5e95a799b8ddcd00222417bf/tasks"
param = "filter=_commongroupId%3Dnull%20AND%20isDone%3Dtrue%20AND%20taskLayer%20IN%20%5B0%2C1%2C2%2C3%2C4%2C5%2C6%2C7%2C8%5D%20AND%20_appId%3D5e95a799b8ddcd00222417bf%20AND%20taskType%20%3D%20bug&_=1588062677081"
cookie = {
    "Hm_lpvt_ec912ecc405ccd050e4cdf452ef4e85a": "1588062474",
    "Hm_lvt_ec912ecc405ccd050e4cdf452ef4e85a": "1,587,603,054,158,760,000,000,000,000,000,000,000,000",
    "TEAMBITION_SESSIONID": "eyJhdXRoVXBkYXRlZCI6MTU4NDA3NzI1MTI4NCwibG9naW5Gcm9tIjoiZGluZ1RhbGsiLCJzdGF0ZSI6e30sInVpZCI6IjVlNmIxOWMzMDNiZDg0ZWFmMDk1NTllNyIsInVzZXIiOnsiX2lkIjoiNWU2YjE5YzMwM2JkODRlYWYwOTU1OWU3IiwibmFtZSI6IuacseaYn+aciCIsImVtYWlsIjoiYWNjb3VudHNfNWU2YjE5YzNmYzNkNDMwMDIwMGExOWQ2QG1haWwudGVhbWJpdGlvbi5jb20iLCJhdmF0YXJVcmwiOiJodHRwczovL3Rjcy50ZWFtYml0aW9uLm5ldC90aHVtYm5haWwvMTExcjlkYmE2OWM5NjEwMjNiODUyNDE1NzI5MzdmMTBmOTdlL3cvMTAwL2gvMTAwIiwicmVnaW9uIjoiY24iLCJsYW5nIjoiIiwiaXNSb2JvdCI6ZmFsc2UsIm9wZW5JZCI6IiIsInBob25lRm9yTG9naW4iOiIxMzExNjc2OTY4NiJ9fQ==",
    "TEAMBITION_SESSIONID.sig": "0gRSkpI8RWi0DrJ4XUGQrRcAQo8",
    "_cio": "623336fd-0ab6-69fd-a319-c744b8a13935",
    "_cioid": "accounts_5e6b19c3fc3d4300200a19d6@mail.teambition.com",
    "_ga": "GA1.2.1891741566.1586866515",
    "_gat": "1",
    "_gid": "GA1.2.355431560.1588050506",
    "mp_eSpCz4lYpMYgtuhdH0F6Wgtt_mixpanel": "%7B%22distinct_id%22%3A%20%22171789d6c94a38-0cbc7f86ca0ecc-6353160-1fa400-171789d6c9598b%22%2C%22displayName%22%3A%20%22rand-alnydg16hz%22%2C%22version%22%3A%20%2211.6.8%22%2C%22userKey%22%3A%20%225e6b19c303bd84eaf09559e7%22%2C%22%24os_version%22%3A%20%22Windows%20NT%2010.0%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%2C%22created_at%22%3A%20%222020-03-13T05%3A27%3A31.284Z%22%2C%22userLanguage%22%3A%20%22zh%22%2C%22env%22%3A%20%22release%22%2C%22daysSinceRegistered%22%3A%2047%2C%22timezone%22%3A%208%2C%22country%22%3A%20%22China%22%2C%22region%22%3A%20%22Zhejiang%22%2C%22experiments%22%3A%20%5B%0A%20%20%20%20%22all_require_member_count.B%22%2C%0A%20%20%20%20%22all_project_template_0413.A%22%2C%0A%20%20%20%20%22all_helpcenter.A%22%2C%0A%20%20%20%20%22all_project_template_algo.B%22%2C%0A%20%20%20%20%22all_code_platform_user_experiment.A%22%2C%0A%20%20%20%20%22171222newuser_web_obd4.A%22%2C%0A%20%20%20%20%22labs_axon.A%22%2C…",
    "mp_tbpanel__c": "4",
    "referral": "%7B%22domain%22%3A%22www.teambition.com%22%2C%22path%22%3A%22%2Fproject%2F5e95a799b8ddcd00222417bc%2Fbug%2Fsection%2Fall%22%2C%22query%22%3A%22%22%2C%22hash%22%3A%22%22%7D",
    "teambition_lang": "zh"
}


result = requests.get(url=url, params=param, cookies=cookie).json()
data = json.dumps(result)

print(data)