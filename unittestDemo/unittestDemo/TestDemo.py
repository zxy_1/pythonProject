import unittest
import HTMLTestRunner

from openpyxl.compat import file


class TestDemo(unittest.TestCase):
    def setUp(self):
        print("测试方法执行：setUp")

    def tearDown(self):
        print("测试方法执行：tearDown")

    def test_demo1(self):
        print("测试放发：test_demo1")

    def test_demo2(self):
        print("测试方法：test_demo2")

    @classmethod
    def setUpClass(cls):
        print("测试方法：setUpClass")

    @classmethod
    def tearDownClass(cls):
        print("测试方法：tearDownClass")


if __name__ == "__main__":
    filePath = "../report/htmlReport.html"
    fp = file(filePath, "wb")
    # unittest.main()
    suite = unittest.TestSuite()
    suite.addTest(TestDemo('test_demo1'))
    # unittest.TextTestRunner.run(suite)
    runner = HTMLTestRunner.HTMLTestRunner(stream=fp, title="this is first test")
    runner.run(suite)
